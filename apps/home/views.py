from django.shortcuts import render
from django.core.exceptions import FieldDoesNotExist


def error404(request):
	return render(request,'errors/404.html', status=404)


def home_view(request):
	return render(request, 'home/home.html', {})
