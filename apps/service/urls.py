from django.urls import path
from apps.service import views


urlpatterns = [
    path('', views.service_view, name='service'),
]