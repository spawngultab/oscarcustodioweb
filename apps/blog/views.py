from django.shortcuts import render, redirect
from django.db.models import Q
from django.core.paginator import Paginator
from django.http import JsonResponse
from .models import Categoria, Post
from .forms import PostForm


def blog_view(request):
	consulta = request.GET.get('buscar')
	categorias = list(Categoria.objects.filter(estatus=True).order_by('nombre').values('id', 'nombre'))
	if consulta:
		posts = Post.objects.filter(Q(titulo__icontains=consulta) | Q(contenido__icontains=consulta)).distinct()
	else:
		posts = list(Post.objects.filter(estatus=True).order_by('-fecha_creacion').values('id', 'titulo', 'nombre_corto', 'contenido', 'imagen', 'autor_id__nombre', 'autor_id__apellido', 'fecha_creacion'))
	paginator = Paginator(posts, 3)
	page = request.GET.get('page')
	posts = paginator.get_page(page)
	return render(request, 'blog/blog.html', {'consulta': consulta, 'posts': posts, 'categorias': categorias})


def categoria_view(request, id):
	consulta = request.GET.get('buscar')
	categorias = Categoria.objects.filter(estatus=True).order_by('nombre').values('id', 'nombre')
	categoria = categorias.filter(id=id).values('nombre')
	nombre_categoria_minusculas = categoria[0]['nombre'].lower()
	if consulta:
		posts = Post.objects.filter(Q(titulo__icontains=consulta) | Q(contenido__icontains=consulta)).distinct()
	else:
		posts = list(Post.objects.filter(categoria_id=id, estatus=True).order_by('-fecha_creacion').values('id', 'titulo', 'nombre_corto', 'contenido', 'imagen', 'autor_id__nombre', 'autor_id__apellido', 'categoria_id', 'categoria__nombre', 'fecha_creacion'))
	paginator = Paginator(posts, 3)
	page = request.GET.get('page')
	posts = paginator.get_page(page)
	return render(request, 'blog/' + nombre_categoria_minusculas + '.html', {'posts': posts, 'categorias': categorias})


def detalle_view(request, id):
	post = list(Post.objects.filter(id=id, estatus=True).values('id', 'titulo', 'nombre_corto', 'contenido', 'imagen', 'autor_id__nombre', 'autor_id__apellido', 'categoria_id', 'fecha_creacion'))
	id_categoria = post[0]['categoria_id']
	categorias = Categoria.objects.filter(estatus=True).order_by('nombre').values('id', 'nombre')
	categoria = categorias.filter(id=id_categoria, estatus=True).values('id', 'nombre')
	return render(request, 'blog/detalle.html', {'post': post, 'categoria': categoria, 'categorias': categorias})


def post_view(request):
	if request.method == 'POST':
		form = PostForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('post')
	else:
		form = PostForm()
	return render(request, 'blog/post.html', {'form': form})


def postComentario_view(request):
	# Corregir ya que todavia no funciona
	if request.method == 'POST':
		post_id = request.POST['post_id']
		print(post_id)
