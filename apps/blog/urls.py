from django.urls import path
from django.contrib.auth.decorators import login_required
from apps.blog import views


urlpatterns = [
    path('', views.blog_view, name='blog'),
    path('detalle/<int:id>/', views.detalle_view, name='detalle'),
    path('post/', login_required(views.post_view), name='post'),
    path('post/comentario/', login_required(views.postComentario_view), name='comentario'),
    path('categoria/<int:id>/', views.categoria_view, name='categoria'),
]