from django import forms
from .models import Post, Comentario


class PostForm(forms.ModelForm):
	class Meta:
		model   = Post
		fields  = '__all__'
		widgets = {
			'titulo': forms.TextInput(attrs={'class': 'form-control'}),
			'nombre_corto': forms.TextInput(attrs={'class': 'form-control'}),
			'descripcion': forms.TextInput(attrs={'class': 'form-control'}),
			'contenido': forms.Textarea(attrs={'class': 'form-control'}),
			'imagen': forms.TextInput(attrs={'class': 'form-control'}),
			'autor': forms.Select(attrs={'class': 'custom-select'}),
			'categoria': forms.Select(attrs={'class': 'custom-select'}),
			'estatus': forms.CheckboxInput(),
		}


class ComentarioForm(forms.ModelForm):
	class Meta:
		model   = Comentario
		fields  = '__all__'
		widgets = {
			'contenido': forms.Textarea(attrs={'class': 'form-control'}),
		}
