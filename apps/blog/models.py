from django.db import models
from ckeditor.fields import RichTextField


class Categoria(models.Model):
	nombre         = models.CharField(max_length=50)
	estatus        = models.BooleanField('Registro activo', default=True)
	fecha_creacion = models.DateField('Fecha de creación', auto_now=False, auto_now_add=True)

	class Meta:
		verbose_name        = 'Categoría'
		verbose_name_plural = 'Categorías'

	def __str__(self):
		return self.nombre


class Autor(models.Model):
	nombre         = models.CharField('Nombre(s)', max_length=255)
	apellido       = models.CharField('Apellido(s)', max_length=255)
	estatus        = models.BooleanField('Registro activo', default=True)
	mail           = models.EmailField('Correo electrónico')
	web            = models.URLField(null=True, blank=True)
	linkedin       = models.URLField(null=True, blank=True)
	git            = models.URLField('GitLab y/o GitHub', null=True, blank=True)
	facebook       = models.URLField(null=True, blank=True)
	twitter        = models.URLField(null=True, blank=True)
	instagram      = models.URLField(null=True, blank=True)
	fecha_creacion = models.DateField('Fecha de registro', auto_now=False, auto_now_add=True)

	class Meta:
		verbose_name        = 'Autor'
		verbose_name_plural = 'Autores'

	def __str__(self):
		return '{0} {1}'.format(self.apellido, self.nombre)


class Post(models.Model):
	titulo         = models.CharField('Título', max_length=100)
	nombre_corto   = models.CharField('Slug', max_length=100)
	descripcion    = models.CharField('Descripción', max_length=110)
	contenido      = RichTextField()
	imagen         = models.URLField()
	autor          = models.ForeignKey(Autor, on_delete=models.CASCADE)
	categoria      = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	estatus        = models.BooleanField('Registro activo', default=True)
	fecha_creacion = models.DateTimeField('Fecha de creación', auto_now=False, auto_now_add=True)

	class Meta:
		verbose_name        = 'Post'
		verbose_name_plural = 'Posts'

	def __str__(self):
		return self.titulo


class Comentario(models.Model):
	post       = models.ForeignKey(Post, on_delete=models.CASCADE)
	comentario = RichTextField()

	class Meta:
		verbose_name        = 'Comentario'
		verbose_name_plural = 'Comentarios'

	def __str__(self):
		return self.post
