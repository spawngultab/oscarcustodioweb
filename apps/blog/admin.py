from django.contrib import admin
from .models import *


class CategoriaAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'fecha_creacion')


class AutorAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'apellido', 'mail', 'fecha_creacion')


class PostAdmin(admin.ModelAdmin):
	list_display = ('titulo', 'descripcion', 'autor', 'categoria', 'estatus')


admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Autor, AutorAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Comentario)