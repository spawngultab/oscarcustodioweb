from django import forms
from .models import Correo


class CorreoForm(forms.ModelForm):
	class Meta:
			model   = Correo
			fields  = '__all__'
			widgets = {
				'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
				'correo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Correo'}),
				'mensaje': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Mensaje', 'rows': '8'}),
			}
