from django.db import models


class Correo(models.Model):
	nombre  = models.CharField(max_length=200, default='')
	correo  = models.EmailField('Correo eléctronico', default='')
	mensaje = models.TextField(max_length=1000, default='')

	class Meta:
		verbose_name = 'Correo'
		verbose_name_plural = 'Correos'

	def __str__(self):
		return self.nombre
