from django.conf import settings
from django.shortcuts import render
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from .forms import CorreoForm


def send_mail(nombre, mail, mensaje):
	context = {'nombre': nombre, 'mail': mail, 'mensaje': mensaje}
	template = get_template('contact/mail.html')
	content = template.render(context)

	email = EmailMultiAlternatives(
		'Correo de contacto',
		'oscarcustodio.site',
		settings.EMAIL_HOST_USER,
		[mail]
	)

	email.attach_alternative(content, 'text/html')
	email.send()


def contact_view(request):
	if request.method == 'POST':
		nombre  = request.POST.get('nombre')
		mail    = request.POST.get('correo')
		mensaje = request.POST.get('mensaje')
		send_mail(nombre, mail, mensaje)
		
		form = CorreoForm(request.POST)
		if form.is_valid():
			form.save()
			return render(request, 'contact/contact.html', {'status': 'ok'})
	else:
		form = CorreoForm()
	return render(request, 'contact/contact.html', {'form': form})
