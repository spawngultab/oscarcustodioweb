from django.urls import path
from apps.aboutme import views


urlpatterns = [
    path('', views.aboutme_view, name='aboutme'),
]